FROM openjdk:latest
COPY target/*.war /home/app/business-api.war
ENTRYPOINT ["java", "-war", "/home/app/business-api.war"]